let chars = {
  chart: {
    type: "column",
    zoomType: "xy"
  },
  credits: {
    enabled: false
  },
  title: {
    text: "data visualization"
  },

  xAxis: {
    title: {
      text: " "
    },
    categories: []
  },

  yAxis: {
    title: {
      text: ""
    }
  },

  series: [
    {
      name: "",
      data: []
    }
  ]
};
function matchesPerYear() {
  fetch("/matchesPerYear")
    .then(res => res.json())
    .then(jsonData => {
      chars.series[0].data = [...Object.values(jsonData)];
      chars.series[0].name = "Matches played";
      chars.xAxis.categories = [...Object.keys(jsonData)];
      chars.xAxis.name = "IPL seasons";
      chars.yAxis.title.text = "Matches played per year";
      chars.title.text = "Matches played per year";
      Highcharts.chart("matchesPerYear", chars);
    });
}
function extraRuns2016() {
  fetch("/extraRuns2016")
    .then(res => res.json())
    .then(jsonData => {
      //console.log(jsonData)
      chars.series[0].data = [...Object.values(jsonData)];
      chars.series[0].name = "Extra Runs given";
      chars.xAxis.categories = [...Object.keys(jsonData)];
      chars.xAxis.name = "";
      chars.yAxis.title.text = "Extra Runs";
      chars.title.text = "Extra Runs Per Year";
      Highcharts.chart("extraRuns2016", chars);
    });
}

function ecoBowlers2015() {
  fetch("/ecoBowlers2015")
    .then(res => res.json())
    .then(jsonData => {
      chars.series = [];
      for (let i = 0; i < jsonData.length; i++) {
        chars.series[i] = {};
        chars.series[i].data = [+jsonData[i][1]];
        chars.series[i].name = jsonData[i][0];
      }
      //console.log(chars.series)
      chars.xAxis.categories = [];
      chars.yAxis.title.text = "Economy";
      chars.title.text = "top-10 Economy Bowler";
      Highcharts.chart("ecoBowlers2015", chars);
    });
}
function matchesWonPerTeamPerYear() {
  fetch("/matchesWonPerTeamPerYear")
    .then(res => res.json())
    .then(jsonData => {
      //console.log(jsonData)
      let teams = {};
      for (let season in jsonData) {
        for (let team in jsonData[season]) {
          teams[team] = [];
        }
      }
      for (let i in jsonData) {
        for (let j in teams) {
          if (jsonData[i][j]) {
            teams[j].push(jsonData[i][j]);
          } else {
            teams[j].push(0);
          }
        }
      }
      chars.chart.type = "column";
      chars.chart.zoomType = "xy";
      chars.credits.enabled = false;
      chars.title.text = "Matches Won Per Year by Every Team";
      chars.xAxis.title.text = "Years";
      chars.xAxis.categories = Object.keys(jsonData);
      chars.yAxis.title.text = "Matches Won Per Team";
      chars.series = [];
      for (let i in teams) {
        chars.series.push({ name: i, data: teams[i] });
      }
      Highcharts.chart("matchesWonPerTeamPerYear", chars);
    });
}
matchesPerYear();
extraRuns2016();
ecoBowlers2015();
matchesWonPerTeamPerYear();
