var http= require('http')
var fs=require("fs")

// http.createServer((req,res)=>{
//      fs.readFile("./index.html","UTF-8",(err,html)=>{
//       res.writeHead(200,{"content-type": "text/html"})
//       res.write(html)
//       res.end()
//       } )
// }).listen(3000)
var server= http.createServer((req,res)=>{
    if(req.url==='/'){
        fs.readFile('./index.html','utf-8',(err,data)=>{
            if(err){
                console.log("file-not exist")
            } else{
                res.writeHead(200,{'Content-type':'text/html'});
                res.write(data);
                res.end();
            }
        })
    }
    else if(req.url==='/matchesPerYear'){
        fs.readFile('../output/matchesPerYear.json','utf-8',(err,data)=>{
            if(err){
                console.log("file-not exist")
            } else{
                res.writeHead(200,{'Content-type':'text/html'});
                res.write(data);
                res.end();
            }
        })
    } else if(req.url==='/matchesWonPerTeamPerYear'){
        fs.readFile('../output/matchesWonPerTeamPerYear.json','utf-8',(err,data)=>{
            if(err){
                console.log("file-not exist")
            } else{
                res.writeHead(200,{'Content-type':'text/html'});
                res.write(data);
                res.end();
            }
        })

    } else if(req.url==='/extraRuns2016'){
        fs.readFile('../output/extraRuns2016 .json','utf-8',(err,data)=>{
            if(err){
                console.log("file-not exist")
            } else{
                res.writeHead(200,{'Content-type':'text/html'});
                res.write(data);
                res.end();
            }
        })
        
    } else if(req.url==='/ecoBowlers2015'){
        fs.readFile('../output/ecoBowlers2015','utf-8',(err,data)=>{
            if(err){
                console.log("file-not exist")
            } else{
                res.writeHead(200,{'Content-type':'text/html'});
                res.write(data);
                res.end();
            }
        })
        
    } else if(req.url==='/app.js'){
        fs.readFile('./app.js','utf-8',(err,data)=>{
            if(err){
                console.log("file-not exist")
            } else{
                res.writeHead(200,{'Content-type':'text/html'});
                res.write(data);
                res.end();
            }
        })
    }
}).listen(3000)
