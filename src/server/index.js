const {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRuns2016,
  ecoBowlers2015,
  runsMadeByDavidWarner
} = require("./ipl.js");

const csvFilePath = "./src/data/matches.csv";
const csv = require("csvtojson");
csv()
  .fromFile(csvFilePath)
  .then(MatchesData => {
    const csvFilePath1 = "./src/data/deliveries.csv";
    const csv1 = require("csvtojson");
    csv1()
      .fromFile(csvFilePath1)
      .then(deliveryData => {
        var a = JSON.stringify(matchesPlayedPerYear(MatchesData), null, 2);
        var b = JSON.stringify(matchesWonPerTeamPerYear(MatchesData), null, 2);
        var c = JSON.stringify(
          extraRuns2016(MatchesData, deliveryData, "2016"),
          null,
          2
        );
        var d = JSON.stringify(
          ecoBowlers2015(deliveryData, MatchesData),
          null,
          2
        );
        var e = JSON.stringify(
          runsMadeByDavidWarner(deliveryData, MatchesData, "DA Warner"),
          null,
          2
        );

        var fs = require("fs");

        fs.writeFile("./src/output/matchesPerYear.json", a, function(err) {
          if (err) console.log(err);
          else console.log("Write operation complete.");
        });
        fs.writeFile("./src/output/matchesWonPerTeamPerYear.json", b, function(
          err
        ) {
          if (err) console.log(err);
          else console.log("Write operation complete.");
        });
        fs.writeFile("./src/output/extraRuns2016 .json", c, function(err) {
          if (err) console.log(err);
          else console.log("Write operation complete.");
        });
        fs.writeFile("./src/output/ecoBowlers2015", d, function(err) {
          if (err) console.log(err);
          else console.log("Write operation complete.");
        });
      });
  });
