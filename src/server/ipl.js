const matchesPlayedPerYear = (matches) => {
    const matchesPlayed = matches.reduce((year, matchesPerYear) => {
        year[matchesPerYear.season] = (year[matchesPerYear.season] || 0) + 1;
        return year;
    }, {});
    return matchesPlayed
}

const matchesWonPerTeamPerYear = matches => {
    let winningTeam = matches.reduce((matchesWonPerYear, year) => {
        if (matchesWonPerYear[year['season']] == undefined) matchesWonPerYear[year['season']] = {}
        matchesWonPerYear[year.season][year.winner] = (matchesWonPerYear[year.season][year.winner] || 0) + 1
        return matchesWonPerYear
    }, {})
    return winningTeam
}

const extraRuns2016 = (matches, delivery, year) => {
    let id = matches.reduce((idArr, matchesInfo) => {
        if (matchesInfo['season'] === year) idArr.push(matchesInfo.id);
        return idArr
    }, []);
    const extRuns = delivery.reduce((teams, runs) => {
        if (id.includes(runs['match_id'])) {
            if (teams.hasOwnProperty(runs['bowling_team'])) {
                teams[runs['bowling_team']] = teams[runs['bowling_team']] + parseInt(runs['extra_runs'])
            }
            else {
                teams[runs['bowling_team']] = parseInt(runs['extra_runs'])
            }
        }
        return teams
    }, {})
    return extRuns;
}

const ecoBowlers2015 = (deliveryData, MatchesData) => {
    let id = MatchesData.reduce((idArr, obj) => {
        if (obj['season'] === "2015") idArr.push(obj.id);
        return idArr
    }, []);
    
    let Runs = deliveryData.reduce((runs,delivery) => {
        if (id.includes(delivery['match_id'])) {
            if (runs.hasOwnProperty(delivery['bowler'])) runs[delivery["bowler"]] += parseInt(delivery["total_runs"]) - parseInt(delivery['bye_runs']) - parseInt(delivery['legbye_runs'])
            else runs[delivery["bowler"]] = parseInt(delivery["total_runs"]) - parseInt(delivery['bye_runs']) - parseInt(delivery['legbye_runs'])
        }
        return runs
    },{})
    // return Runs
    let overs =deliveryData.reduce((over, delivery) => {
        if (id.includes(delivery['match_id'])) {
            if (delivery.wide_runs == 0 && delivery.noball_runs == 0) over[delivery.bowler] = (over[delivery.bowler] || 0) + 1 / 6;
        }
        return over
    },{})
    //return overs
    let eco = {}
    for (let ball in overs) {
        eco[ball] = (Runs[ball] / Math.round(overs[ball])).toFixed(2)
    }
    return Object.entries(eco).sort((a, b) => { return a[1] - b[1] }).slice(0, 10)
}


//IPL-5   

const runsMadeByDavidWarner = (deliveries, matches, batsman) => {
    const deliveriesPlayed = deliveries.filter(delivery => delivery.batsman === batsman)
    
    const id = deliveriesPlayed.reduce((idArr, deliveries) => {
        if (!(idArr.includes(deliveries.match_id))) idArr.push(deliveries.match_id)
        return idArr
    }, [])
    //return id

    const runsPerMatch = deliveriesPlayed.reduce((batsmanRunsPerDeliveries, deliveries) => {
        if (id.includes(deliveries.match_id)) {        
            if (batsmanRunsPerDeliveries.hasOwnProperty(deliveries.match_id)) batsmanRunsPerDeliveries[deliveries.match_id] += parseInt(deliveries['batsman_runs'])
            else batsmanRunsPerDeliveries[deliveries.match_id] = parseInt(deliveries['batsman_runs'])
        }
        return batsmanRunsPerDeliveries

    }, {})
    //return runsPerMatch

    const seasons = matches.reduce((matchIdPerSeason, season) => {
        if (id.includes(season.id)) matchIdPerSeason[season.id] = parseInt(season['season'])
        return matchIdPerSeason
    }, {})
    //return seasons

    const totalRunsPerSeason = Object.keys(seasons).reduce((returnArray,id)=>{
        if (returnArray.hasOwnProperty(seasons[id])) {
            returnArray[seasons[id]] += parseInt(runsPerMatch[id])
        } else returnArray[seasons[id]] = parseInt(runsPerMatch[id])
        return returnArray
    },{})

    return totalRunsPerSeason
}
//ecoBowlers2015(deliveryData,MatchesData)

module.exports = {
    matchesPlayedPerYear,
    matchesWonPerTeamPerYear,
    extraRuns2016,
    ecoBowlers2015,
    runsMadeByDavidWarner
}


